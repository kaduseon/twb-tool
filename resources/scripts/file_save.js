/*
Kaduseon's tunawithbacon.com Tool
Version 03-202001251622
*/

sfileSubmit = document.getElementById("file-save-submit");

function csvify(table) {
    var csvstr = null;

    // set colum headers
    csvstr = table[0][0];
    for (var c = 1; c < table[0].length; c++) {
        csvstr += "," + table[0][c];
    }
    csvstr += "\n";

    var eventstr = "";
    if (areAllFromSameEvent()) {
        eventstr += table[1][0] + ",";
        eventstr += table[1][1] + ",";
        eventstr += table[1][2] + ",";
        eventstr += table[1][3] + ",";
        eventstr += table[1][4] + ",";
    }

    for (var r = 1; r < table.length; r++) {
        var rowstr = null;
        // copy event details from appropriate place
        if (allSameEvent) {
            // copy from the first entry/eventstr
            rowstr = eventstr;
        } else {
            // copy from this entry
            rowstr = "";
            rowstr += table[r][0] + ","; // event is a string
            rowstr += table[r][1] + ","; // date is a date (effectively string here)
            rowstr += REGIONS[table[r][2]] + ","; // region is a selection
            rowstr += table[r][3] + ","; // netplay is a selection but the index is correct
            rowstr += table[r][4] + ","; // version is left as a string
        }

        rowstr += table[r][5] + ","; // player 1 name (string)
        rowstr += CHARACTER_CODES[table[r][6]] + ","; // player 1 char 1 (selection)
        rowstr += CHARACTER_CODES[table[r][7]] + ","; // player 1 char 2 (selection)
        rowstr += CHARACTER_CODES[table[r][8]] + ","; // player 1 char 3 (selection)

        rowstr += table[r][9] + ","; // player 2 name (string)
        rowstr += CHARACTER_CODES[table[r][10]] + ","; // player 2 char 1 (selection)
        rowstr += CHARACTER_CODES[table[r][11]] + ","; // player 2 char 2 (selection)
        rowstr += CHARACTER_CODES[table[r][12]] + ","; // player 2 char 3 (selection)

        rowstr += table[r][13]; // video (string)

        // don't add newline if last entry
        if (r < table.length - 1) {
            rowstr += "\n";
        }

        csvstr += rowstr;
    }

    return csvstr;
}

function _onSaveFile() {
    var data = getMatchEntryData();

    var datablob = new Blob([csvify(data)]);
    var objurl = URL.createObjectURL(datablob, {type : 'text/plain'});

    var temp = document.createElement("a");
    temp.href = objurl;
    temp.download = "twb.csv";

    document.body.appendChild(temp);

    temp.click();

    document.body.removeChild(temp);

    URL.revokeObjectURL(objurl);
}

sfileSubmit.setAttribute("onclick", "_onSaveFile()");