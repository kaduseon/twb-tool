/*
Kaduseon's tunawithbacon.com Tool
Version 03-202001251622
*/

//==============================================================================
//
//  HTML ELEMENT HANDLING AND STUFF
//
//==============================================================================
// Used for making it a little easier when making the input fields.

SUBGRID_ENTRY_CLASS_KEY   = "match-grid-entry-fieldkey";
SUBGRID_ENTRY_CLASS_VALUE = "match-grid-entry-fieldvalue";

// Creates a text field.
function createMatchEntryItem_TextInputField(matchEntry, entry, idx, col) {
    var matchEntryItem;
    var matchEntryInputField;

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_KEY);
    matchEntryItem.innerHTML = COLUMN_TO_FIELD_NAME[col];
    matchEntry.appendChild(matchEntryItem);

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_VALUE);

    matchEntryInputField = document.createElement("input");
    matchEntryInputField.type = "text";
    matchEntryInputField.value = entry[col];
    matchEntryInputField.size = 40;

    matchEntryInputField.setAttribute("oninput", "setEntryValue(" + idx + ", \"" + col + "\", this.value)");

    matchEntryItem.appendChild(matchEntryInputField);
    matchEntry.appendChild(matchEntryItem);
}



// Creates a selection box for character, region, netplay, and stuff.
// Also attaches the corresponding universal set event.
// Also returns the selection box (needed for a future feature).
function createMatchEntryItem_Select(matchEntry, entry, idx, col, selectOptions, reverseLookupSelect = null) {
    var matchEntryItem;

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_KEY);
    matchEntryItem.innerHTML = COLUMN_TO_FIELD_NAME[col];
    matchEntry.appendChild(matchEntryItem);

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_VALUE);

    var entrySelection = document.createElement("select");
    for (i = 0; i < selectOptions.length; i++) {
        var opt = document.createElement("option");
        opt.appendChild(document.createTextNode(selectOptions[i]));
        opt.value = i;

        entrySelection.appendChild(opt);
    }

    if (reverseLookupSelect != null) {
        entrySelection.selectedIndex = reverseLookupSelect.indexOf(entry[col]);
    } else {
        entrySelection.selectedIndex = entry[col];
    }

    entrySelection.setAttribute("onchange", "setEntryValue(" + idx + ", \"" + col + "\", this.value)");

    matchEntryItem.appendChild(entrySelection);
    matchEntry.appendChild(matchEntryItem);

    return entrySelection;
}



// Creates a date field.
function createMatchEntryItem_Date(matchEntry, entry, idx, col) {
    var matchEntryItem;
    var matchEntryInputField;

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_KEY);
    matchEntryItem.innerHTML = COLUMN_TO_FIELD_NAME[col];
    matchEntry.appendChild(matchEntryItem);

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_VALUE);

    matchEntryInputField = document.createElement("input");
    matchEntryInputField.type = "date";
    matchEntryInputField.value = entry[col];

    matchEntryInputField.setAttribute("onchange", "setEntryValue(" + idx + ", \"" + col + "\", this.value)");

    matchEntryItem.appendChild(matchEntryInputField);
    matchEntry.appendChild(matchEntryItem);
}



// Adds the delete button to delete an entry.
function createMatchEntryItem_Delete(matchEntry, idx) {
    var matchEntryItem;
    var matchEntryInputField;

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_KEY);
    matchEntryItem.innerHTML = "Delete this entry?";
    matchEntry.appendChild(matchEntryItem);

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_VALUE);
    matchEntryInputField = document.createElement("button");
    matchEntryInputField.setAttribute("onclick", "deleteEntry(" + idx + ")");
    matchEntryInputField.appendChild(document.createTextNode("Delete"));
    matchEntryItem.appendChild(matchEntryInputField);
    matchEntry.appendChild(matchEntryItem);
}

// Adds the delete button to delete an entry.
function createMatchEntryItem_Reset(matchEntry, idx) {
    var matchEntryItem;
    var matchEntryInputField;

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_KEY);
    matchEntryItem.innerHTML = "Reset this entry?";
    matchEntry.appendChild(matchEntryItem);

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_VALUE);
    matchEntryInputField = document.createElement("button");
    matchEntryInputField.setAttribute("onclick", "resetEntry(" + idx + ")");
    matchEntryInputField.appendChild(document.createTextNode("Reset"));
    matchEntryItem.appendChild(matchEntryInputField);
    matchEntry.appendChild(matchEntryItem);
}





// Adds an "empty" row between elements to give some visual space,
// hopefully making it easier to read.
function createMatchEntryItem_Spacer(matchEntry) {
    var matchEntryItem;

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_KEY);
    matchEntryItem.appendChild(document.createElement("br"));
    matchEntry.appendChild(matchEntryItem);

    matchEntryItem = document.createElement("div");
    matchEntryItem.classList.add(SUBGRID_ENTRY_CLASS_VALUE);
    matchEntryItem.appendChild(document.createElement("br"));
    matchEntry.appendChild(matchEntryItem);
}





/*----------------------------------------------------------------------------*/

mainMatchGrid = document.getElementById("match-grid-main");

function _buildMatchEntry(entry, idx, allSameEvent = false) {
    var matchEntry = document.createElement("div");
    matchEntry.classList.add("match-grid-entry");

    if (!allSameEvent || idx == 1) {
        // if all matches inherit from same event, then use firt entry to input
        // all of the event details

        // event > event
        createMatchEntryItem_TextInputField(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["Event"]);
        // event > date
        createMatchEntryItem_Date(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["Date"]);
        // event > region
        createMatchEntryItem_Select(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["Region"], REGIONS);
        // event > netplay
        createMatchEntryItem_Select(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["Netplay"], NETPLAY);
        // event > version
        createMatchEntryItem_TextInputField(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["Version"]);

        createMatchEntryItem_Spacer(matchEntry);
    }

    // this always needs to be written out individually
    // event > video
    createMatchEntryItem_TextInputField(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["Video"]);

    createMatchEntryItem_Spacer(matchEntry);

    // do player stuff
    // player > P1Name
    createMatchEntryItem_TextInputField(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["P1Name"]);
    // player > P1Char1
    createMatchEntryItem_Select(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["P1Char1"], CHARACTER_NAMES);
    // player > P1Char2
    createMatchEntryItem_Select(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["P1Char2"], CHARACTER_NAMES);
    // player > P1Char3
    createMatchEntryItem_Select(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["P1Char3"], CHARACTER_NAMES);

    createMatchEntryItem_Spacer(matchEntry);

    // player > P2Name
    createMatchEntryItem_TextInputField(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["P2Name"]);
    // player > P2Char1
    createMatchEntryItem_Select(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["P2Char1"], CHARACTER_NAMES);
    // player > P2Char2
    createMatchEntryItem_Select(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["P2Char2"], CHARACTER_NAMES);
    // player > P2Char3
    createMatchEntryItem_Select(matchEntry, entry, idx, FIELD_KEY_TO_COLUMN["P2Char3"], CHARACTER_NAMES);


    createMatchEntryItem_Spacer(matchEntry);
    createMatchEntryItem_Reset(matchEntry, idx);
    createMatchEntryItem_Spacer(matchEntry);
    createMatchEntryItem_Delete(matchEntry, idx);

    mainMatchGrid.appendChild(matchEntry);
}





/*----------------------------------------------------------------------------*/





function buildMatchEntries(table, allSameEvent = false) {
    while (mainMatchGrid.firstChild) {
        mainMatchGrid.removeChild(mainMatchGrid.firstChild);
    }

    for (var r = 1; r < table.length; r++) {
        _buildMatchEntry(table[r], r, allSameEvent);
    }
}