/*
Kaduseon's tunawithbacon.com Tool
Version 03-202001251622
*/

CHARACTER_NAMES = [
        "None",
        "Filia",
        "Cerebella",
        "Peacock",
        "Parasoul",
        "Ms. Fortune",
        "Painwheel",
        "Valentine",
        "Double",
        "Squigly",
        "Big Band",
        "Eliza",
        "Fukua",
        "Beowulf",
        "Robo-Fortune"
    ];

CHARACTER_CODES = [
        "N",
        "FI",
        "CE",
        "PC",
        "PS",
        "MF",
        "PW",
        "VA",
        "DB",
        "SQ",
        "BB",
        "EL",
        "FU",
        "BW",
        "RF"
    ];

REGIONS = [
        "North America",
        "Europe",
        "Japan + Korea",
        "South America",
        "Oceania"
    ];

NETPLAY = [
        "Offline / Local",
        "Netplay"
    ];


COLUMN_TO_FIELD_KEY = {
    0 : "Event",
    1 : "Date",
    2 : "Region",
    3 : "Netplay",
    4 : "Version",
    5 : "P1Name",
    6 : "P1Char1",
    7 : "P1Char2",
    8 : "P1Char3",
    9 : "P2Name",
    10 : "P2Char1",
    11 : "P2Char2",
    12 : "P2Char3",
    13 : "Video"
}

COLUMN_TO_FIELD_NAME = {
    0: 'Event',
    1: 'Date',
    2: 'Region',
    3: 'Netplay',
    4: 'Version',
    5: 'Player 1 Name',
    6: 'Character #1',
    7: 'Character #2',
    8: 'Character #3',
    9: 'Player 2 Name',
    10: 'Character #1',
    11: 'Character #2',
    12: 'Character #3',
    13: 'Video'
}

/* this one isn't strictly necessary, but it's nice to work with */
FIELD_KEY_TO_COLUMN = {
    "Event" : 0,
    "Date" : 1,
    "Region" : 2,
    "Netplay" : 3,
    "Version" : 4,
    "P1Name" : 5,
    "P1Char1" : 6,
    "P1Char2" : 7,
    "P1Char3" : 8,
    "P2Name" : 9,
    "P2Char1" : 10,
    "P2Char2" : 11,
    "P2Char3" : 12,
    "Video" : 13
}

EXPECTED_COLUMN_COUNT = Object.keys(COLUMN_TO_FIELD_KEY).length;