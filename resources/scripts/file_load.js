/*
Kaduseon's tunawithbacon.com Tool
Version 03-202001251622
*/

/*
    Put this script at END of <body>

*/

var lfileInputHasEverything = true;

if (!window.File) {
    console.log("Missing window.File - cannot load saved lfiles.");
    lfileInputHasEverything = false;
}

if (!window.FileReader) {
    console.log("Missing window.FileReader - cannot load saved lfiles.");
    lfileInputHasEverything = false;
} 

if (!window.Blob) {
    console.log("Missing window.Blob - cannot load saved lfiles.");
    lfileInputHasEverything = false;
 }





/*----------------------------------------------------------------------------*/





/*
    Variables to handle lfile input.
*/
var lfileSelect = document.getElementById("file-load-select");
var lfileReader = new FileReader();

var inputFile = null;
var lfileData = null;

var onFileLoadListener = null;





/*----------------------------------------------------------------------------*/





/*
    Functions to handle lfile input events.
*/
function _fileSelect(event) {
    inputFile = event.target.files[0];

    lfileReader.readAsText(inputFile);
}

function _verifyLoadedData(data) {
    for (var r = 0; r < data.length; r++) {
        var row = data[r];
        var cols = row.length;

        if (cols != EXPECTED_COLUMN_COUNT) {
            window.alert("Could not verify file: row #" + r + " had wrong number of columns (" + cols + " found, expected " + EXPECTED_COLUMN_COUNT + ")");
            return false;
        }
    }

    return true;
}

function _fileReader_onLoad(event) {
    /*
    VERY PRIMITIVE WAY OF READING A CSV!
    WILL NOT WORK IF ANY FIELDS HAVE COMMAS
    */

    var text = lfileReader.result;
    var lines = text.split("\n");
    var rows = [];
    for (var i = 0; i < lines.length; i++) {
        var row = lines[i].split(",");
        rows.push(row);
    }

    if (_verifyLoadedData(rows)) {
        lfileData = rows;
    } else {
        return;
    }
    
    if (onFileLoadListener != null) {
        onFileLoadListener(lfileData);
    }
}


function _fileReader_onError(event) {
    window.alert("An error occurred while reading the file:\n" + reader.error.message);
}





/*----------------------------------------------------------------------------*/





/*
    Attach functions to inputs events.
*/

lfileSelect.setAttribute("onchange", "_fileSelect(event)");
lfileReader.onload = _fileReader_onLoad;
lfileReader.onerror = _fileReader_onError;

function getFileData() {
    return lfileData;
}

function setOnFileLoadListener(func) {
    onFileLoadListener = func;
}