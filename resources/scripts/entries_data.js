/*
Kaduseon's tunawithbacon.com Tool
Version 03-202001251622
*/

function _makeHeaderRow() {
    return ["Event", "Date", "Region", "Netplay", "Version", "P1Name", "P1Char1", "P1Char2", "P1Char3", "P2Name", "P2Char1", "P2Char2", "P2Char3", "URL"]
}

function _makeDefaultRow() {
    return ["", "2020-01-01", 0, 1, "2E+ Final", "", 0, 0, 0, "", 0, 0, 0, ""];
}

function _makeDefaultTable() {
    var table = [];

    table.push(_makeHeaderRow());
    table.push(_makeDefaultRow());
    table.push(_makeDefaultRow());

    return table;
}





/*----------------------------------------------------------------------------*/





settingsElementAllSameEvent = document.getElementById("settings-all-same-event");





/*----------------------------------------------------------------------------*/




var allSameEvent = true;
var matchEntryData = _makeDefaultTable();

// when loading an existing file, this checks if they're all from the same
// event and sets the "all same event" flag accordingly
function _areAllLoadedEventsTheSame(data) {
    /*
    Magic number 4: The first 5 fields (0 to 4 inclusive) are the event
    details that are shared across all of them.
    */
    for (var r = 1; r < data.length; r++) {
        for (var c = 0; c <= 4; c++) {
            if (data[1][c] != data[r][c]) {
                return false;
            }
        } 
    }

    return true;
}



function areAllFromSameEvent() {
    return allSameEvent;
}



function loadMatchEntryData() {
    var confirmed = confirm("Are you sure you want to load this file? The previous table will be lost, and this cannot be undone.");
    if (!confirmed) {
        return;
    }

    matchEntryData = getFileData();

    /*
        fix selections codes to selection indices
    */
    for (var r = 1; r < matchEntryData.length; r++) {
        matchEntryData[r][2] = REGIONS.indexOf(matchEntryData[r][2]);

        matchEntryData[r][6] = CHARACTER_CODES.indexOf(matchEntryData[r][6]);
        matchEntryData[r][7] = CHARACTER_CODES.indexOf(matchEntryData[r][7]);
        matchEntryData[r][8] = CHARACTER_CODES.indexOf(matchEntryData[r][8]);
        matchEntryData[r][10] = CHARACTER_CODES.indexOf(matchEntryData[r][10]);
        matchEntryData[r][11] = CHARACTER_CODES.indexOf(matchEntryData[r][11]);
        matchEntryData[r][12] = CHARACTER_CODES.indexOf(matchEntryData[r][12]);
    }

    allSameEvent = _areAllLoadedEventsTheSame(matchEntryData);
    settingsElementAllSameEvent.checked = allSameEvent;
    buildMatchEntries(matchEntryData, allSameEvent);
}



function getMatchEntryData() {
    console.log('getting ' + matchEntryData);
    return matchEntryData;
}



function deleteEntry(idx) {
    if (matchEntryData.length <= 2) {
        alert("The last entry cannot be deleted.");
        return;
    }

    var confirmed = confirm("Are you sure you want to delete this entry?");
    if (!confirmed) {
        return;
    }

    /*
    Magic number 4: The first 5 fields (0 to 4 inclusive) are the event
    details that are shared across all of them.
    */
    if (allSameEvent && idx == 1 && matchEntryData.length > 2) {
        for (var c = 0; c <= 4; c++) {
            matchEntryData[2][c] = matchEntryData[1][c];
        }
    }

    matchEntryData.splice(idx, 1);
    buildMatchEntries(matchEntryData, allSameEvent);
}

setOnFileLoadListener(loadMatchEntryData);
buildMatchEntries(matchEntryData, allSameEvent);





/*----------------------------------------------------------------------------*/





function resetEntry(idx) {
    var resetReference = _makeDefaultRow();

    for (var c = 0; c < matchEntryData[idx].length; c++) {
        matchEntryData[idx][c] = resetReference[c];
    }

    // this is really lazy and expensive
    buildMatchEntries(matchEntryData, allSameEvent);
}



function setEntryValue(idx, col, val) {
    matchEntryData[idx][col] = val;
}



function createNewEntry() {
    matchEntryData.push(_makeDefaultRow());
    buildMatchEntries(matchEntryData, allSameEvent);
}





/*----------------------------------------------------------------------------*/



function onAllSameEventChange(checkbox) {
    allSameEvent = checkbox.checked;
    buildMatchEntries(matchEntryData, allSameEvent);
}

settingsElementAllSameEvent.setAttribute("onchange", "onAllSameEventChange(this)");