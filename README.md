# How to use:
Download link:
https://gitlab.com/kaduseon/twb-tool/-/archive/master/twb-tool-master.zip

Download and keep all stuff together (the tool.html file and the resources
folder in the same directory).

Open tool.html in your browser.

It *should* work as-is just from your browser.

Tested in Waterfox 2020.01 (64-bit).

If you find any issues or have any ideas of what I can do better, let me know
at Kaduseon#1337 on Discord.

**This is the first uploaded version, and this has some significant changes
in logic relative to the previous two major versions of this concept, so there
may be issues I still have to fix. Please let me know if you find any so I can
address them.**

# Versions:
## v03-202001251622
- Fixed: default/sample entry had version set as "2E+Final", meant to be "2E+ Final" (missing space)

## v03-202001241545

**Known issues**:
- If you reload the page, some fields may look set but they aren't (hard refresh
should remove this false data)
- Data verification is very primitive - doesn't check fields are valid/correct,
only checks for number of columns
- Code is total spaghetti and will be improved in the future hopefully (mostly
useful for those who are learning to code and want reference materials, or those
who want to try and derive something from this)

# Et cetera:
You can study this code to learn from it and use it for whatever, but if you
used it with little to no modification, crediting me (Kaduseon) would be
appreciated.

If you'd like to share some love, sharing the tool around with people who you
think it would help would be much appreciated.

This is a tool made for the Skullgirls community. If you run or are involved
with a community project and you think some sort of programming can help, maybe
contact me and we can talk about it - I'm interested in seeing what I can do
to forward communities with this sort of stuff. (I also do hobbyist game dev.)

If you'd like to show further support, any donations to me on Ko-fi would be
greatly appreciated: https://ko-fi.com/kaduseon